## Installation de docker Community Linux
```bash
sudo apt-get update
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
```
## Installation de docker toolbox
Se trouve dans le projet git en .exe
## Installation Docker-Compose
```bash
sudo curl -L "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```
si probléme vérifier votre chemin.  /usr/bin Mettre un lien symbolique 
```bash
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose 
```
## Teste installation 
```bash
docker-compose --version
```
## Désinstallation
```bash
sudo rm /usr/local/bin/docker-compose
pip uninstall docker-compose
```
## Lancer le projet avec docker-compose.yml
Se mettre dans le dossier docker
```bash
docker-compose up -d
```
option -d pour lancer en arriere plan
## Lancer le projet sur docker-Tollbox windows
```bash
docker-compose -f ./docker-compose-Windows.yml up -d
```