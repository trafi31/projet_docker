<?php

$value = "World";

$db = new PDO('mysql:host=database;dbname=mydb;charset=utf8mb4', 'myuser', 'secret');

$databaseTest = ($db->query('SELECT name, database_id, create_date FROM sys.databases'))->fetchAll(PDO::FETCH_OBJ);

?>

<html>
    <body>
        <h1>Hello, <?= $value ?>!</h1>

        <?php foreach($databaseTest as $row): ?>
            <p> <?= $row->name ?></p>
            <p> <?= $row->database_id ?></p>
        <?php endforeach; ?>
    </body>
</html>
