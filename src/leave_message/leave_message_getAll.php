<?php
    //Connection
    try{
    	$bdd = new PDO('mysql:host=database;dbname=mydb;charset=utf8mb4', 'myuser', 'secret');
    }catch(Exception $e){
            die('Erreur : '.$e->getMessage());
    }
    
    $reponse = $bdd->query('SELECT pseudo, message FROM messages_chat');
    echo '<h2>Tout les messsages</h2>';
    //Affichage 
    while ($donnees = $reponse->fetch()){
    	echo '<p><strong>' . htmlspecialchars($donnees['pseudo']) . '</strong> : ' . htmlspecialchars($donnees['message']) . '</p>';
    }
    $reponse->closeCursor();
?>